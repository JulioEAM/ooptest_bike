# frozen_string_literal: true

module MultiSource
  class Base
    TYPE_MAPPINGS = {
      ams: :auction_vehicle,
      dms: :dealer_vehicle,
      third_party: :third_party_vehicle,
    }

    cattr_accessor :multisource_methods, default: []
    cattr_accessor :aggregate, default: false

    class << self; alias_method :aggregate?, :aggregate; end

    def self.fallback(order, aggregate: false)
      @@multisource_methods = order.map { |o| TYPE_MAPPINGS[o] }
      @@aggregate = aggregate
      # multisource_methods = [auction_vehicle, dealer_vehicle, third_party_vehicle]
      # aggregate = false
    end

    def initialize(parent, method_name=nil)
      @parent = parent
      @method_name = method_name
      # @parent = Auction::Vehicle => (ams, dms, tp)
      # @method = nil

      #nuevo object multisource_condition_report
      #@parent = multisource_vehicle2
      #@method_name = :condition_report
    end

    #Llamada a multisource_vehicle.condition_report.grade
    # multisource_vehicle.make
    # multisource_vehicle.fallback_make
    # multisource_vehicle.condition_report
    # multisource_vehicle.fallback_condition_report
    # multisource_vehicle.condition_report.grade
    # multisource_vehicle.fallback_condition_report.grade

    # fallback_method  -> Regrese el primer método que exista no importa de qué vehículo (current_sale_listing)
    # fallback_field   -> Regresa el método aplicado al primer vehículo que exista (make)

    def method_missing(method_name, *args, &block)
      method_name_without_prefix = method_name.gsub(/fallback_/, '')
      if multisource_class?
        klass = "MultiSource::#{@parent.class.reflect_on_association(method_name_without_prefix).klass.name.demodulize}".constantize
        klass.new(self, method_name_without_prefix)
      else
        multisource_method(method_name, *args, &block)
      end
    end

    def multisource_class?
      return true if method_name.start_with?(/fallback_/) && active_record_or_assoc?
      return true if active_record_and_assoc?
    end

    return true if fb && (assoc || ar)

#    return true if fb && ar
#    return true if fb && assoc

    return true if assoc && ar
    false
#    return true if assoc && ar

    def active_record_and_assoc?
      @parent.is_a?(ActiveRecord::Base) &&
      @parent.class.reflect_on_association(method_name_without_prefix).klass.name
    end

    def active_record_or_assoc?
      @parent.is_a?(ActiveRecord::Base) ||
      @parent.class.reflect_on_association(method_name_without_prefix).klass.name
    end

    def multisource_method(method_name, *args, &block)
      if self.class.aggregate?
        aggregate_method(method_name, *args, &block)
      elsif method_name.start_with?(/fallback_/)
        fallback_method(method_name, *args, &block)
      else
        fallback_field(method_name, *args, &block)
      end
    end

    def vehicles
      self.class.multisource_methods.map { |method| @parent.send(method) }.compact
    end

    def fallback_field(method_name, *args, &block)
      vehicles.first.public_send(method_name, *args, &block)  # multisource_vehicle.condition_report
    end

    def fallback_method(method_name, *args, &block)
      vehicles.each do |obj|
        # obj = MultiSource_vehicle.auction_vehicle
        # obj = MultiSource_vehicle.auction_vehicle.condition_report
        obj = obj.public_send(@method_name) if @method_name
        next unless obj
        # obj = condition_report
        val = obj.public_send(method_name, *args, &block)
        # val = condition_report.grade
        return val unless val.nil?
      end
      return nil
    end

    def aggregate_method(method_name, *args, &block)
      vehicles.each_with_object([]) do |obj, array|
        obj = obj.public_send(@method_name) if @method_name
        next unless obj
        array.push(*obj.public_send(method_name, *args, &block))
      end
    end

    def respond_to_missing?(method_name, include_private = false)
      has_method = if @method_name
        parent.public_send(@method_name).respond_to?(method_name)
      else
        @parent.respond_to?(method_name)
      end
      has_method || super
    end
  end
end