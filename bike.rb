# frozen_string_literal: true

# Bicicle class
class Bicycle
  attr_reader :sp_chain_size, :sp_wheel_size
  attr_accessor :size, :sp_tape_color

  def initialize(size = 'M', tape_color = 'blue')
    @size = size
    @sp_tape_color = tape_color
    @sp_wheel_size = '700x25'
    @sp_chain_size = '6,7 and 8 steps'
  end

  def spares
    needed_spares = "you'll need:\n"
    instance_variables.each do |var|
      if var.to_s.start_with?('@sp_')
        needed_spares += "  - A #{instance_variable_get(var)} #{var.to_s.split('_')[1]}\n"
      end
    end
    needed_spares
  end

  def tape_color
    @sp_tape_color
  end
end

# Mechanic class
class Mechanic
  attr_accessor :bikes
  attr_reader :name

  def initialize(name = 'Mechanic_' + @mec_count.to_s)
    @bikes = []
    @mec_count.nil? ? @mec_count = 0 : (@mec_count += 1 if name.start_with?('Mechanic_'))
    @name = name
  end

  def spares
    puts "------ #{@name} you have #{@bikes.count} bikes to fix ------"
    @bikes.each do |bike|
      puts 'For your ' + bike.size + ' size bike ' + bike.spares
    end
  end
end

tiernito = Mechanic.new('Armando')
tiernito.bikes << Bicycle.new('Ch', 'yellow')
tiernito.bikes << Bicycle.new('G', 'red')
tiernito.bikes << Bicycle.new('Xg')

javi = Mechanic.new('Javier')
javi.bikes << Bicycle.new('XCh', 'green acua')
javi.bikes << Bicycle.new

tiernito.spares

javi.spares

mec_x = Mechanic.new
mec_y = Mechanic.new
puts "Mec count: #{mec_x.name}, #{mec_y.name}"

# estoy en master

# tiernito&.non_existing
# tiernito.try(non_existing)
# tiernito.try!(non_existing)

# nil&.non_existing
# nil.try(non_existing)
# nil.try!(non_existing)

# FIELDS = {
#   metodo1: '{ |vehicle| vehicle.nested1.nested2 }',
#   metodo2: '{ |vehicle| vehicle.nested1.nested2 }',
# }

# FIELDS.each do |k, v|
#   p k
#   p v.to_b
# end
